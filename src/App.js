import './App.css';
import { Link } from 'react-router-dom';
import Footer from './modules/footer';
import Routes from './routes';
import Header from './modules/header';

function App() {
  return (
    <>
      {/* header */}
      <Header />
      {/* body */}
      <Routes />
      {/* footer */}
      <Footer />
    </>
  );
}

export default App;
