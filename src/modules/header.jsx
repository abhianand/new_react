import { Link } from "react-router-dom"

const Header = () => {
    return (
        <ul>
            <li>
                <Link to="/amisha" > Amisha </Link>
            </li>
            <li>
                <Link to="/prasobh" > Prasobh </Link>
            </li>
            <li>
                <Link to="/remesh" > Remesh </Link>
            </li>
            <li>
                <a href='/'>Home</a>
            </li>
        </ul>
    )
}

export default Header