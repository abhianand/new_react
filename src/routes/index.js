import { Suspense, lazy } from "react"
import { Route, Routes } from "react-router-dom"
import Loader from "../modules/loader"
import Ramesh from "../pages/ramesh"

const Amisha = lazy(() => import('../pages/amisha'))
const Prasobh = lazy(() => import('../pages/prasobh'))
const Home = lazy(() => import('../pages/home'))

const MyRoutes = [
    {
        path: '/amisha',
        component: Amisha
    },
    {

        
        path: '/prasobh',
        component: Prasobh
    },
    {
        path: '/',
        component: Home
    },
    {
        path: '/remesh',
        component: Ramesh
    }
]

const CustomRoutes = () => {
    return (
        <Suspense fallback={<Loader />}>
            <Routes>
                {
                    MyRoutes.map((r, i) =>{
                        return <Route Component={r.component} path={r.path} />
                    })
                }
            </Routes>
        </Suspense>
    )
}

export default CustomRoutes;